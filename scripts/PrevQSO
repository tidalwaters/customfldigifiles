#!/bin/bash - 
#===============================================================================
#
#          FILE: PrevQSO
# 
#         USAGE: Run this file as an fldigi <EXEC> macro, this script will not run stand alone due to use of 
#					FLDIGI variables.
# 
#   DESCRIPTION: When run from an fldigi macro this script will search the log for previous QSO's with the 
#					current station.  Then a message will be printed to the fldigi TX window depending on
#					the results.
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Laurence McArthur (wa4caw), lmcarthur@gmail.com
#       CREATED: 12/25/2012 02:46:17 PM EST
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#===============================================================================
#  GLOBAL DECLARATIONS
#===============================================================================
call=${FLDIGI_LOG_CALL}
path2log="$FLDIGI_CONFIG_DIR/logs/"
log="merged_recs.adif"                          # This should be derived from an FLDIGI variable, if it becomes available,
												#  but for now you have to edit this variable to name your current log.

#===============================================================================
#  MAIN SCRIPT
#===============================================================================
hits=$(grep -ciw ${call} ${path2log}${log})      # number of log entries for this call sign

if [[ ${hits} -eq 0 ]] ; then                   # if no previous hits on the call in log then we're finished here
	echo "According to my log this is our first QSO on ${FLDIGI_MODEM}, I am pleased to meet you."
	exit
fi

#-------------------------------------------------------------------------------
#  Generate a list of contacts with the calling station, then select the most recent.
#-------------------------------------------------------------------------------
 # :TODO: this section could easily pick contacts by the current operating mode also
qsolist=$(grep -iw ${call} ${path2log}${log})    # make list of contacts with the callsign
lastqsodate=$(awk 'BEGIN {FS="[<,>]"; max=0} {if ($11>max) max=$11} END {print max}' < <(echo "$qsolist")) # extract the last QSO_DATE field from record
lastqsolistlength=$(grep -ciw "${call}.*${lastqsodate}" ${path2log}${log}) # number of qso's on the last qso date
lastqso=$(awk -v call=${call} -v last=${lastqsodate} 'BEGIN {RS="<EOR>" ; search=call".*"last } ; $0 ~ search ' ${path2log}${log})

if [[ ${lastqsolistlength} -gt 1 ]] ; then          # if only one entry for the date there is no need to compare times
	lastqsotime=$(awk 'BEGIN {FS="[<,>]"; max=0} {if ($17>max) max=$17} END {print max}' < <(echo "$lastqso"))
	lastqso=$(awk -v call=${call} -v last=${lastqsodate} -v time=${lastqsotime} 'BEGIN {RS="<EOR>" ; search=call".*"last".*"time } ; $0 ~ search ' ${path2log}${log})
fi
lastqso=$( sed 's/[<,>]/\n/g' < <(echo ${lastqso})) # convert from 1 line to each field on separate line preceeded by field ID
	#Note: I chose to use sed to extract the data fields below, which works better with the data fields as lines.  
	#Alternatively, awk would be a good choice to extract the data making this conversion step unnecessary. 


#-------------------------------------------------------------------------------
#  Extract the desired last qso data and print message.
#-------------------------------------------------------------------------------
#datelast=$(date -d $(sed -n '/QSO_DATE:/{n;p;}' < <(echo "${lastqso}")) "+%a %b %e, %Y") # prints the line after the field ID regex
	#above line has different date format than below, otherwise is same
datelast=$(date -d $(sed -n '/QSO_DATE:/{n;p;}' < <(echo "${lastqso}")) "+%D") # prints the line after the field ID regex
timeon=$(printf "%.4s\n" $(sed -n '/TIME_ON:/{n;p;}' < <(echo "${lastqso}")))
freq=$(printf "%.3f\n" $(sed -n '/FREQ:/{n;p;}' < <(echo "${lastqso}")))   
band=$(printf "%s\n" $(sed -n '/BAND:/{n;p;}' < <(echo "${lastqso}")))     
mode=$(printf "%s\n" $(sed -n '/MODE:/{n;p;}' < <(echo "${lastqso}")))     

#echo "My log shows this is QSO number $(( ${hits} + 1 )) with you, the last was $datelast at $timeon UTC on $freq MHz."
echo "My log shows this is QSO $(( ${hits} + 1 )) with you, the last was $datelast $timeon hrs UTC, ${mode} on ${band} band."
echo "Nice to see you again..."
exit
