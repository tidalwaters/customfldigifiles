#!/bin/bash - 
#===============================================================================
#
#          FILE: chgmacrokeys.sh
# 
#         USAGE: ./chgmacrokeys.sh 
# 
#   DESCRIPTION: Simple script to use with fldigi to move an existing macro to another macro key location.
#
#
#        AUTHOR: Laurence McArthur (wa4caw), lmcarthur@gmail.com
#
#		   NOTE: I wrote this for my own use and primarily to learn about using sed and select in scripts.  
#					You are welcome to use it, learn from my mistakes, or whatever.  Please don't ask me
#					to make changes just for you, or complain to me if it doesn't do what you want.  I am not a 
#					programmer or software developer.  There are no guarantees or warranties, just my hopes that you
#					find some usefulness from this.  Having said that, I would very much enjoy hearing from you if you 
#					used this and found it to be useful, or have ideas to improve, or better even if you send me an
#					improved version.
#
#				 This script was written on, and tested only on a Debian linux system in a 'normal' BASH environment.  
#				 I don't know what will happen on your computer.  I MIGHT work.
#
#       CREATED: 12/05/2012 01:20:52 PM EST
#
#===============================================================================

#----TODO-List------------------------------------------------------------------
#
#	-make optional command line parameters that will bypass the interactive portion
#	-make choice to copy or move from one mdf file to another
#  
#-------------------------------------------------------------------------------
 
 
#----README----------------------------------------------------------------------
#  If you move or copy an empty macro key to another key you will erase anything that previously existed at the new location.
#	This script does a bit of checking but mostly it's up to you to make sure you hit the right keys.  When the 
#	script ends your macro file has already been written.  You have backups don't you?
#	Have fun with this but be careful, I'm not watching you and I assume no responsibility for what happens.  Don't use
#	this if you don't feel comfortable with it.  You have backups don't you?
#	ENJOY!
#-------------------------------------------------------------------------------

macroroot="${HOME}/.fldigi/macros/"  # This is the fldigi default, but change as necessary for your system. 
										#NOTE: Don't use ~ (tilde), it won't be expanded by the below script.
PS3="Choose number, then ENTER: "    # This is the prompt user sees when given choices in the following select commands.


#-------------------------------------------------------------------------------
#  Pick the macro file to edit.
#-------------------------------------------------------------------------------
files="$(ls ${macroroot}*.mdf)"                 # list of macro files in the macro directory
arrfiles=( $files )                             # characterize files to array so it is easy to count them, 
numfiles=${#arrfiles[@]}                        # then count number in array so select will have input bounds

if [[ ${numfiles} -eq 0 ]] ; then               # check that .mdf files exist in the directory
	echo
	echo "ERROR: There are no macro files in ${macroroot}, the macro directory."
	echo
	exit
fi

echo
echo
echo "************************************************************************************************"
echo ">>> WARNING:  If fldigi is running, save the macro file(s) NOW else you may lose any changes. <<<"
echo "              It much safer to use this script on macro files that are not currently in use."
echo "************************************************************************************************"
echo
echo "Important NOTE: Once a macro is moved you must load/reload the macro file in fldigi to see the changes."
echo "			HINT: You can write a simple macro to reload or open a different macro file."
echo
echo "Which macro file to edit? Enter number followed by [Enter]."

select mfile in ${files} ; do
	if [[ $REPLY -ge 1 && $REPLY -le ${numfiles} ]] ; then # limit valid input to the range defined by number of files
		echo
		echo "Now editing macro file:  ${mfile##*/}"
		echo
		break
	fi
done

while true ; do                                 # begin of MAIN while loop
#-------------------------------------------------------------------------------
#  Choose MOVE or COPY or DELETE mode.
#-------------------------------------------------------------------------------
	echo "Do you want to MOVE or COPY or DELETE a macro?"
	while true ; do                                 # begin of MODE loop
		select mode in "move" "copy" "delete" "Exit with no changes" ; do
			case ${mode} in
				"move")
					break;;
				"copy")
					break;;
				"delete")
					break;;
				"Exit with no changes")
					exit;;
				*)
					echo
					echo "Try again, you did not make a valid selection";;
			esac    # --- end of case ---
		done
		
#-------------------------------------------------------------------------------
#  Choose the key location you want to change.
#-------------------------------------------------------------------------------
		mlocstart=0
		while true ; do
			until [[ ${mlocstart} -ge 1 && ${mlocstart} -le 48 ]] ; do
				echo
				echo "Which key do you want to ${mode} FROM? Enter number from 1 - 48, followed by [Enter]."
				read mlocstart
			done
			
#-------------------------------------------------------------------------------
#  Confirm that chosen key location is correct.
#-------------------------------------------------------------------------------
		startstring=$(sed -n "/^\/\/ Macro # ${mlocstart}$/,/^\/\// "'{ /^$\|^\/\//!p }' ${mfile}) # see note below 
																			#for explaination of this weird looking command

			echo
			echo
			echo "**************************************************"
			echo "**************************************************"
			echo "You have chosen to ${mode} this macro:"
			echo "Note: the first line is the macro label (number is macro key minus 1)"
			echo
			echo "${startstring}"
			echo "**************************************************"
			echo "**************************************************"
			echo
			echo "${mode} macro key number $mlocstart.  Is this correct?"
			
			select opt in "Yes" "Choose again" "Exit with no changes"; do
			    case $opt in
			        "Yes")
			            break;;
			        "Choose again")
						mlocstart=0
						echo
						break;;
			        "Exit with no changes")
			            exit;;
			        *)
			            ;;
			    esac    # --- end of case ---
			done
			
			if [[ $opt = "Yes" ]]; then
				break
			fi
		
		done
		mlabelstart=$((mlocstart - 1))              # the labels are 1 number less than the macro key location
		
		
		### for delete mode move directly to cleanup section now, otherwise continue with destination selection
		if [[ ${mode} = "delete" ]] ; then
			break                             # exit the while loop to skip over destination selection and writing 
		fi
		
#		startstring=$(sed -n "/^\/\/ Macro # ${mlocstart}$/,/^\/\// "'{ /^$\|^\/\//!p }' ${mfile}) # see later note about this stmt
		
#------------------------------------------------------------------------------
#  Choose the destination key location.
#-------------------------------------------------------------------------------
		mlocfinal=0
		while true ; do
		
			until [[ ${mlocfinal} -ge 1 && ${mlocfinal} -le 48 && ${mlocfinal} -ne ${mlocstart} ]] ; do
				echo
				echo "Which key do you want to ${mode} key ${mlocstart} TO? Enter number from 1 - 48, followed by [Enter]."
				read mlocfinal  
				if [[ ${mlocfinal} -eq ${mlocstart} ]]; then
					echo
					echo "ERROR: You must enter a different key to ${mode} TO.  Try again."
				fi
			done
		
#-------------------------------------------------------------------------------
#  Confirm that the chosen key location is correct.
#-------------------------------------------------------------------------------
			echo
			echo "${mode} macro key number ${mlocstart} to key ${mlocfinal}  Is this correct?"
			select opt in "Yes" "Choose again" "Exit with no changes"; do
				case $opt in
					Yes)
						break;;
					"Choose again")
						mlocfinal=0
						echo
						break;;
					"Exit with no changes")
						exit;;
					*)
						;;
				esac    # --- end of case ---
			done
			
					
#-------------------------------------------------------------------------------
#  Check if the destination key location is empty and, if so, deal with it.
#-------------------------------------------------------------------------------
			if [[ $opt = "Yes" ]]; then
		
	## The sed stmt selects the range from the final loc number to the start of next macro, 
	## then prints only non-empty, non-commented lines.
	## The funny quoting in the following sed command is cuz of bash's treatment of the ! character (history expansion).
	## Some of the sed command needs double quotes (for variable expansion) and some needs single quotes (so ! won't expand).
					finalstring=$(sed -n "/^\/\/ Macro # ${mlocfinal}$/,/^\/\// "'{ /^$\|^\//!p }' ${mfile})
					
					if [[ -n ${finalstring} ]] ; then               # if the location is NOT empty...then
						echo
						echo "Your destination key # ${mlocfinal} is already occupied, what do you wish to do?"
						select opt in "Choose a different final location" "Forget my choice, just use the next empty key location" "Overwrite the existing macro at this location" "Exit and do nothing" ; do
							case ${opt} in
								"Choose a different final location")
									mlocfinal=0
									echo
									break;;             # break form this select and return to choose final location
								"Forget my choice, just use the next empty key location")
									while [[ -n ${finalstring} ]] ; do # search for the next empty location
										mlocfinal=$((mlocfinal + 1)) # increment the final location up 1 number
										finalstring=$(sed -n "/^\/\/ Macro # ${mlocfinal}$/,/^\/\// "'{ /^$\|^\//!p }' ${mfile})
									done
									echo
									echo "Key location ${mlocfinal} is the next empty key"
									locgud=y
									break;;
								"Overwrite the existing macro at this location")
									locgud=y
									break;;
								"Exit and do nothing")
									exit ;;
								*)
									;;
							esac    # --- end of case ---
						done	# end of select
					else
						break                           # exit while loop if mlocfinal IS empty alredy
					fi
		
					if [[ ${locgud} = "y" ]] ; then
						break                           # break from the while loop
					fi
			fi
		
		done   # end of while loop
		mlabelfinal=$((mlocfinal - 1))              # the labels are 1 number less than the macro location
		
		
#-------------------------------------------------------------------------------
#  Now do the move, we have the locations all chosen and qualified.
#-------------------------------------------------------------------------------
		# Delete any exiting text at the final location, including the macro label line.
		sed -i "/^\/\/ Macro # ${mlocfinal}$/,/^\/\// "'{ /^$\|^\/\//!d }' ${mfile}
		
		# Append the macro text to the final location.
		IFS=$(echo -en "\n")                            # set IFS to newline only else read will drop trailing spaces
		while read -r line ; do
			line=$(sed "/^\/\\$ / s/${mlabelstart}/${mlabelfinal}/" < <(echo -n ${line})) 	# in the label line, 
																							#chg the number to final
			line=$(sed 's/\\/\\\\/g' < <(echo -n ${line})) 				# escape backslashes so they will be 
																		#escaped in the below sed command
# This line maybe not needed since added the double backslash in next line???			
#line=$(sed '/^\\\\n/ s/\\/\\\\/' < <(echo -n ${line})) 		# add extra backslash if the line begins 
																			#with \n for the below sed command which 
																			#wants double escapes on a backslash
			sed -i "/^\/\/ Macro # ${mlocfinal}$/a \\${line}" ${mfile} 	# the double backslash before the ${line} allows 
																			#the sed append command to preserve 
																			#leading whitespaces and leading \n's
		
		done < <(echo ${startstring} | tac)   	# reverse the line order so appending each line after 
												#the macro start line will end up correct
		break               # exit MODE while loop
	done                    # end of MODE while loop
	
#-------------------------------------------------------------------------------
#  Finally, cleanup the mess you made and print SUCCESS message.
#-------------------------------------------------------------------------------
	echo
	if [[ ${mode} = "move" || ${mode} = "delete" ]] ; then
		# move or delete:
		# Delete the macro text from the start location, including label line.
		# Then put back a blank label line so this slot available.
		sed -i "/^\/\/ Macro # ${mlocstart}$/,/^\/\// "'{ /^$\|^\/\//!d }' ${mfile}
		sed -i "/^\/\/ Macro # ${mlocstart}$/a /$ ${mlabelstart} \n" ${mfile}
		if [[ ${mode} = "move" ]] ; then
			echo "You successfully moved macro # ${mlocstart} TO macro # ${mlocfinal}"
		else
			echo "You successfully deleted macro # ${mlocstart}"
		fi
	else
		# copy:
		echo "You successfully copied macro # ${mlocstart} TO macro # ${mlocfinal}"
	fi
	
	echo "--->  Don't forget to reload the macro file for it to take effect"
	echo
	
#-------------------------------------------------------------------------------
#  Choose to change another macro or exit.
#-------------------------------------------------------------------------------
	select opt in "Return to change another macro" "I'm done, exit" ; do
		case ${opt} in
			"Return to change another macro")
				echo
				break;;                             # return to beginning, pick another macro to change
			"I'm done, exit")
				exit;;                              # finished
			*)
				echo
				echo "Invalid input, try again";;
		esac    # --- end of case ---
	done       # end of select
done       # end of MAIN while loop, return to beginning of script for a rerun

exit

